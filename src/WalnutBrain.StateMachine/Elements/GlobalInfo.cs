﻿using System;

namespace WalnutBrain.StateMachine.Internal
{
    internal class GlobalInfo
    {
        public Type Type { get; set; }
        public Func<object, object> Initializer { get; set; }
        public object DefaultValue { get; set; }
        public object Value { get; set; }
    }
}