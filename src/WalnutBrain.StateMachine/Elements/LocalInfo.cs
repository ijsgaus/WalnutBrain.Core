﻿using System;

namespace WalnutBrain.StateMachine.Internal
{
    internal class LocalInfo
    {
        public bool Permanent { get; set; }
        public bool ReadOnly { get; set; }
        public object DefaultValue { get; set; }
        public Func<object, object> Initializator { get; set; }
        public object Value { get; set; }

    }

    
}