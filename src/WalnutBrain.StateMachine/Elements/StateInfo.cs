﻿using System;
using System.Collections.Concurrent;

namespace WalnutBrain.StateMachine.Internal
{
    public class StateInfo<TState> where TState : IEquatable<TState>
    {
        public StateInfo(TState state, string displayName = null, Type stateHelperType = null)
        {
            _displayName = displayName;
            State = state;
            StateHelperType = stateHelperType;
            Declared = true;
        }


        public TState State { get; private set; }

        public string DisplayName
        {
            get { return _displayName ?? State.ToString(); }
            
        }

        public Type StateHelperType { get; private set; }

        internal bool Declared { get; set; } 

        private readonly string _displayName;

        private ConcurrentDictionary<string, LocalInfo> _locals = new ConcurrentDictionary<string, LocalInfo>();

        

    }

    

    
}