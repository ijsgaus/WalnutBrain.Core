﻿namespace WalnutBrain.StateMachine
{
    public interface IEventDetail
    {
        void Init(object data);
    }
}