﻿using System;
using System.Collections.Generic;
using WalnutBrain.StateMachine.Internal;

namespace WalnutBrain.StateMachine
{
    public abstract class StateMachineBuilder<TMachine, TState, TEvent> 
        : IMachineBuilder<TMachine, TState, TEvent> 
        where TEvent : IEquatable<TEvent> 
        where TState : IEquatable<TState> 
        where TMachine : StateMachine<TState, TEvent>
    {
        public IStateBuilder<TMachine, TState, TEvent> State(TState state, string displayName = null)
        {
            return new StateBuilder<TMachine, TState, TEvent>(this, RegisterState(state, displayName, null));
        }

        private StateInfo<TState> RegisterState(TState state, string displayName, Type stateHelperType)
        {
            StateInfo<TState> info;
            if (!States.TryGetValue(state, out info))
            {
                info = new StateInfo<TState>(state, displayName, stateHelperType);
                States.Add(state, info);
            }
            else
            {
                info.Declared = true;
            }
            return info;
        }

        public IStateBuilder<TMachine, TState, TEvent, TStateHelper> State<TStateHelper>(TState state, string displayName = null) where TStateHelper : StateHelper<TMachine, TState, TEvent>
        {
            
            return new StateBuilder<TMachine, TState, TEvent, TStateHelper>(this, RegisterState(state, displayName, typeof(TStateHelper)));
        }

        public IMachineBuilder<TMachine, TState, TEvent> Global<TGlobal>(string name, TGlobal @default = default(TGlobal))
        {
            if(Globals.ContainsKey(name))
                throw new StateMachineBuilderException("Global with name {0} already registered".AsFormat(name));
            Globals.Add(name, new GlobalInfo
            {
                DefaultValue = @default,
                Initializer = null,
                Type = typeof(TGlobal),
                Value = null
            });
            return this;
        }

        public IMachineBuilder<TMachine, TState, TEvent> Global<TGlobal>(string name, Func<IInitGlobalContext<TMachine, TState, TEvent>, TGlobal> initializer)
        {
            if (Globals.ContainsKey(name))
                throw new StateMachineBuilderException("Global with name {0} already registered".AsFormat(name));
            Globals.Add(name, new GlobalInfo
            {
                DefaultValue = null,
                Initializer = o1 => (object) initializer((IInitGlobalContext<TMachine, TState, TEvent>) o1),
                Type = typeof(TGlobal),
                Value = null
            });
            return this;
        }

        protected abstract void Configure();

        public void Build(TMachine machine)
        {
            throw new NotImplementedException();
        }

        internal Dictionary<TState, StateInfo<TState>> States =
            new Dictionary<TState, StateInfo<TState>>();
        internal Dictionary<string, GlobalInfo> Globals  =
            new Dictionary<string, GlobalInfo>(StringComparer.InvariantCultureIgnoreCase);
    }
}