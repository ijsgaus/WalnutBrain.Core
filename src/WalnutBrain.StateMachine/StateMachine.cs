﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using WalnutBrain.StateMachine.Internal;

namespace WalnutBrain.StateMachine
{
    public class StateMachine<TState, TEvent>
        where TState : IEquatable<TState>
        where TEvent : IEquatable<TEvent>
    {
        private readonly ConcurrentDictionary<string, GlobalInfo> _globals;
        

        public StateMachine()
        {
            _globals = new ConcurrentDictionary<string, GlobalInfo>(StringComparer.InvariantCultureIgnoreCase);
            
        }

        internal void Add(string name, GlobalInfo globalInfo)
        {
            _globals.TryAdd(name, globalInfo);
        }
        
        public object GetValue(string name)
        {
            GlobalInfo info;
            if(!_globals.TryGetValue(name, out info))
                throw new StateMachineException("Unknown global variable {0}".AsFormat(name));
            return info.Value;
        }

        public T GetValue<T>(string name)
        {
            GlobalInfo info;
            if (!_globals.TryGetValue(name, out info))
                throw new StateMachineException("Unknown global variable {0}".AsFormat(name));
            if(!typeof(T).IsAssignableFrom(info.Type))
                throw new StateMachineException("Invalid global variable type request {0}, type is {1}, requested {2}".AsFormat(name, info.Type, typeof(T)));
            return (T) info.Value;
        }

        public void SetValue(string name, object value)
        {
            GlobalInfo info;
            if (!_globals.TryGetValue(name, out info))
                throw new StateMachineException("Unknown global variable {0}".AsFormat(name));
            if (!info.Type.IsInstanceOfType(value))
                throw new StateMachineException("Invalid global variable type assigment {0}, type is {1}, requested {2}".AsFormat(name, info.Type, value == null ? "(null)" : value.GetType().FullName));
            info.Value = value;
        }
    }
}