using System;
using System.Collections.Generic;
using WalnutBrain.StateMachine.Internal;

namespace WalnutBrain.StateMachine
{
    internal class StateBuilder<TMachine, TState, TEvent> : StateBuilder<TState>, IStateBuilder<TMachine, TState, TEvent>
        where TEvent : IEquatable<TEvent>
        where TState : IEquatable<TState>
        where TMachine : StateMachine<TState, TEvent>

    {
        private readonly StateMachineBuilder<TMachine, TState, TEvent> _machineBuilder;

        public StateBuilder(StateMachineBuilder<TMachine, TState, TEvent> machineBuilder, StateInfo<TState> stateInfo) : base(stateInfo)
        {
            _machineBuilder = machineBuilder;
        }

        public IStateBuilder<TMachine, TState, TEvent> Local<TLocal>(string name, TLocal @default = default(TLocal), bool permanent = false,
            bool readOnly = false)
        {
            AddLocal(name, @default, null, permanent, readOnly, false);
            return this;
        }

        public IStateBuilder<TMachine, TState, TEvent> Local<TLocal>(string name, Func<IEnterStateContext<TMachine, TState, TEvent>, TLocal> initializer = null, bool permanent = false, bool readOnly = false)
        {
            AddLocal(name, null, initializer, permanent, readOnly, false);
            return this;
        }

        public IStateBuilder<TMachine, TState, TEvent> AfterEnter(Action<IEnterStateContext<TMachine, TState, TEvent>> onEnter)
        {
            throw new NotImplementedException();
        }

        private List<Action<TMachine, TState, TEvent>>  
    }

    internal class StateBuilder<TMachine, TState, TEvent, TStateHelper> : StateBuilder<TState>, IStateBuilder<TMachine, TState, TEvent, TStateHelper>
        where TEvent : IEquatable<TEvent>
        where TState : IEquatable<TState>
        where TMachine : StateMachine<TState, TEvent>
        where TStateHelper : StateHelper<TMachine, TState, TEvent>
    {
        private readonly StateMachineBuilder<TMachine, TState, TEvent> _machineBuilder;

        public StateBuilder(StateMachineBuilder<TMachine, TState, TEvent> machineBuilder, StateInfo<TState> stateInfo) : base(stateInfo)
        {
            _machineBuilder = machineBuilder;
        }

        public IStateBuilder<TMachine, TState, TEvent, TStateHelper> Local<TLocal>(string name, TLocal @default = default(TLocal), bool permanent = false,
            bool readOnly = false)
        {
            AddLocal(name, @default, null, permanent, readOnly, true);
            return this;
        }

        public IStateBuilder<TMachine, TState, TEvent, TStateHelper> Local<TLocal>(string name, Func<IEnterStateContext<TMachine, TState, TEvent, TStateHelper>, TLocal> initializer = null, bool permanent = false, bool readOnly = false)
        {
            AddLocal(name, null, initializer, permanent, readOnly, true);
            return this;
        }

        internal Dictionary<string, LocalInfo> Locals =
            new Dictionary<string, LocalInfo>(StringComparer.InvariantCultureIgnoreCase);
    }

    internal abstract class StateBuilder<TState>
        where TState : IEquatable<TState>
    {
        public StateBuilder(StateInfo<TState> stateInfo)
        {
            StateInfo = stateInfo;
        }

        protected void AddLocal(string name, object @default, object initializer, bool permanent, bool readOnly, bool withStateHelper)
        {
            LocalInfo desc;
            if (Locals.TryGetValue(name, out desc))
                throw new StateMachineBuilderException("Local with name {0} in state {1} already registered".AsFormat(name,
                    StateInfo.State));
            Locals.Add(name, new LocalInfo
            {
                Permanent = permanent,
                ReadOnly = readOnly,
                DefaultValue = @default,
                Initializator = initializer,
                WithStateHelper = withStateHelper
            });
        }

        internal Dictionary<string, LocalInfo> Locals =
            new Dictionary<string, LocalInfo>(StringComparer.InvariantCultureIgnoreCase);

        public StateInfo<TState> StateInfo { get; private set; }
    }
}